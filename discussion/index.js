// console.log("Hello World")
// Comments in Java Script
// -Two write comments we use two forward slash for single line comments and two forward slash and two asterisk for multiline comments
// -this is single line comments
/* This is a multi line comments 
to write them, we add two asterisk inside of the forward slashes and write
comments in between

single line = ctrl + /
multi line = ctrl + shift + /

*/

/*
Variable

-variable contain values that can be changed over the execution
line of the program
- To declare a variable, we use the "let"keyword
-pag "let"pwedeng paltan yung name ng variable
-pag "cons"(constant) hindi pwedeng paltan yung name


-si "var"nagagamit hoisting

-scope nasasakupan ng code
-local - "let"
-globat -"var"


declaration - wala pang value
initialization - may value na

"=" assignment operator

re asign = nag palit ng value



Rules

Let
asignment operator



code Blocking - pag matagal mag load yung codes

*/


/*let productName ='desktop computer';
console.log(productName);
productName='cellphone';
console.log(productName);


let productPrice = 500;
console.log(productPrice);
productPrice = 450;
console.log(productPrice);
*/

// Constants
/*
- use contants for the values that will not change.
*/
/*const deliveryFee = 30;
console.log(deliveryFee);*/




// Data Types
// 6 data type lang kay javasript
/*
1. Strings
- Strings = are series of characters that create a word,
a phrase, sentence, or anything related to "text"
- Strings in javascript can be written using a single quote ('') 
or a ("") double quote can be used for creating strings.




*/

let country ='Philippines';
let province ="MEtro Manila";

// CONCATENATION
console.log(country +", "+ province);


// Numbers
/*
	- - Includes integers / whole numbers, decimal numbers
	fraction, exponential rotation.

*/
let headCounnt = 26;
console.log(headCounnt);

let grade = 98.7;
console.log(grade);

let planetDistancec = 2e10;
console.log(planetDistancec);

let PI = 22/7;
console.log(PI);

console.log(Math.PI);

console.log("John's grade last quarter is:" + grade);

// Boolean
/*
- Boolean's values are logical values.
-Booklean values can contain either "true" or "false"

*/
let isMarried = false;
let isGoodConduct =true;
console.log("isMarried " + isMarried);
console.log("isGoodConduct " + isGoodConduct);
console.log(isMarried);
console.log(isGoodConduct);

// Objects

		// Arrays
		/*
		-they are a special kind of data that stores
		multiple values.
		- they are special type of an object.
		- they can store different data types but is normally
		used to store similar data types.

		-([]) box array

		*/

		// Syntax:
			//let/const arrayName = [ElementA, ElementB,ElementC]; 

		let grades = [98.7, 92.1, 90.2, 94.6];
		console.log(grade);
		console.log(grades[0]);

		let userDetails = ["john", "smith", 32, true];
		console.log(userDetails);


		// Object Literals
		/*
				- Objects are another special kind of data type
				that mimics real world objects/items

				-They area used to create complex data that contains 
				pieces of information that are relevant to each other

				- every individual piece of information if called a property
				of the objects

				Syntex:
				let/const objectName = {
				propertyA: value,
				propertyB: value

				}


				OBject (property then value fullName: 'Juan Dela Cruz')

		*/

		let personDetails = {
		fullName: 'Juan Dela Cruz',
		age: 35, 
		isMarried: false,
		contact: ['+639876543210', '024785425'],
		address: {
			houseNumber: '345',
			city: 'Manila'
		}
	}
	console.log(personDetails)